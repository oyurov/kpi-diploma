import {Component, OnInit} from '@angular/core';
import {ChartService} from './service/chart.service';
import * as CanvasJS from '../assets/canvasjs.min';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})

export class AppComponent implements OnInit {
  factories = [];
  dataToRender = [];
  y = 1;
  t0 = 0;
  t = 12;
  h = 1;
  id: number = null;
  name: string;
  alpha: number;
  a: number;
  b: number;
  e: number;
  p: number;

  ngOnInit(): void {
    this.calc();
  }

  constructor(private chartService: ChartService) {
    this.chartService.find().subscribe(res => {
      res.forEach(fac => this.factories.push(fac));
    });
  }

  calc(): void {
    const dataPoints = [];

    const request = {
      y: this.y,
      t0: this.t0,
      t: this.t,
      h: this.h
    };

    this.chartService.calculate(request).subscribe(res => {
      let result = Object.entries(res.y);
      // tslint:disable-next-line:only-arrow-functions
      result.sort(function (a, b) {
        // @ts-ignore
        return a[1] - b[1];
      });
      for (let [key, value] of result) {
        let x = parseFloat(key);
        let y = value;
        dataPoints.push({x, y});
      }
      this.dataToRender.push({
        type: 'line',
        name: 'Y(t)',
        showInLegend: true,
        markerSize: 0,
        dataPoints: dataPoints
      });
      for (let fac of res.factories) {
        let resultP = Object.entries(fac.resultP);
        resultP.sort(function (a, b) {
          // @ts-ignore
          return a[1] - b[1];
        });
        let dataToPass = [];
        for (let [key, value] of resultP) {
          let x = parseFloat(key);
          let y = value;
          dataToPass.push({x, y});
        }
        this.dataToRender.push({
          type: 'line',
          name: fac.name,
          showInLegend: true,
          markerSize: 0,
          dataPoints: dataToPass
        });
      }
    });
    console.log(this.dataToRender);
  }

  showChart(): void {

    this.calc();

    const chart = new CanvasJS.Chart('chartContainer', {
      zoomEnabled: true,
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: 'Розрахунки прибутку підприємст'
      },
      toolTip: {
        shared: true
      },
      legend: {
        cursor: 'pointer',
        verticalAlign: 'top',
        horizontalAlign: 'center',
        dockInsidePlotArea: true
      },
      data: this.dataToRender
    });

    chart.render();

    this.dataToRender = [];

    window.scrollTo(0, document.body.scrollHeight);
  }

  addFactory(): void {
    const factoryToSave = {
      id: this.id,
      name: this.name,
      alpha: this.alpha,
      a: this.a,
      b: this.b,
      e: this.e,
      p: this.p,
    };
    this.chartService.save(factoryToSave).subscribe(res => console.log(res));
    window.location.reload();
  }

  changeFactory(index: number): void {
    const factory = this.factories[index];

    this.id = factory.id;
    this.name = factory.name;
    this.alpha = factory.alpha;
    this.a = factory.a;
    this.b = factory.b;
    this.e = factory.e;
    this.p = factory.p;

  }

  deleteFactory(index: number): void {
    this.chartService.delete(this.factories[index].id).subscribe(res => console.log(res));
    window.location.reload();
  }

  autoCalc(): void {
    const request = {
      y: this.y,
      t0: this.t0,
      t: this.t,
      h: this.h
    };

    this.chartService.autoCalc(request).subscribe(res => console.log(res));

    this.showChart();
  }

  updateGraph(): void {
    window.location.reload();
  }
}
