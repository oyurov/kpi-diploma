import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

export interface CalcRequest {
  y: number;
  t0: number;
  t: number;
  h: number;
}

export interface Factory {
  name: string;
  alpha: number;
  a: number;
  b: number;
  e: number;
  p: number;
}

@Injectable({
  providedIn: 'root'
})
export class ChartService {

  constructor(private http: HttpClient) {
  }

  calculate(calcRequest: CalcRequest): Observable<any> {
    return this.http.post<any>('/factory/calc', calcRequest);
  }

  autoCalc(calcRequest: CalcRequest): Observable<any> {
    return this.http.post<any>('/factory/calc/auto', calcRequest);
  }

  save(factory: Factory): Observable<any> {
    return this.http.post<any>('factory', factory);
  }

  find(): Observable<any> {
    return this.http.get<any>('factory');
  }

  delete(id: string): Observable<any> {
    return this.http.delete<any>('factory/' + id);
  }
}
