package org.kpi.diploma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DiplomaKpiBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(DiplomaKpiBackendApplication.class, args);
    }

}
