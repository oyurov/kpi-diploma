package org.kpi.diploma.controller;

import org.kpi.diploma.common.service.FactoryService;
import org.kpi.diploma.common.dto.CalcRequestDto;
import org.kpi.diploma.common.model.Factory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
public class FactoryController {

    private final FactoryService factoryService;

    @Autowired
    public FactoryController(FactoryService factoryService) {
        this.factoryService = factoryService;
    }

    @PostMapping("/factory")
    public ResponseEntity<?> saveFactory(@RequestBody Factory factory) {
        return new ResponseEntity<>(factoryService.save(factory), HttpStatus.OK);
    }

    @GetMapping("/factory")
    public ResponseEntity<?> findAll() {
        return new ResponseEntity<>(factoryService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/factory/{factoryName}")
    public ResponseEntity<?> findFactoryByName(@PathVariable("factoryName") String factoryName) {
        return new ResponseEntity<>(factoryService.findByName(factoryName), HttpStatus.OK);
    }

    @PutMapping("/factory")
    public ResponseEntity<?> updateFactory(@RequestBody Factory factory) {
        return new ResponseEntity<>(factoryService.save(factory), HttpStatus.OK);
    }

    @PostMapping("/factory/calc")
    public ResponseEntity<?> calculate(@RequestBody CalcRequestDto request) {
        return new ResponseEntity<>(factoryService.calculate(request.getY(), request.getT0(), request.getT(), request.getH()), HttpStatus.OK);
    }

    @PostMapping("/factory/calc/auto")
    public ResponseEntity<?> autoCalc(@RequestBody CalcRequestDto request) {
        return new ResponseEntity<>(factoryService.autoCalc(request.getY(), request.getT0(), request.getT(), request.getH()), HttpStatus.OK);
    }

    @DeleteMapping("/factory/{id}")
    public ResponseEntity<?> deleteFactoryById(@PathVariable("id") String id) {
        return new ResponseEntity<>(factoryService.delete(UUID.fromString(id)), HttpStatus.OK);
    }
}
