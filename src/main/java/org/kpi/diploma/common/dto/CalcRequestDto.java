package org.kpi.diploma.common.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CalcRequestDto implements Serializable {
    private double y;
    private double t0;
    private double t;
    private double h;
}
