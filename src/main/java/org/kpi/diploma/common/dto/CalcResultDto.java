package org.kpi.diploma.common.dto;

import lombok.Data;
import org.kpi.diploma.common.model.Factory;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Data
public class CalcResultDto implements Serializable {
    private List<Factory> factories = new LinkedList<>();
    private Map<Double, Double> y = new ConcurrentHashMap<>();
}
