package org.kpi.diploma.common.data.id;

import java.util.UUID;

public interface HasUUID {
    UUID getId();
}
