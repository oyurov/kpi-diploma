package org.kpi.diploma.common.service;

import lombok.extern.slf4j.Slf4j;
import org.kpi.diploma.dao.FactoryDao;
import org.kpi.diploma.common.dto.CalcResultDto;
import org.kpi.diploma.common.model.Factory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Service
@Slf4j
public class FactoryServiceImpl implements FactoryService {

    private static boolean AUTO_CALC = false;
    private static boolean AUTO_CORRECTION = false;

    private static List<Double> tempResultY = new LinkedList<>();

    @Autowired
    private FactoryDao factoryDao;

    @Override
    public Factory save(Factory factory) {
        log.trace("Executing save...");
        return factoryDao.save(factory);
    }

    @Override
    public Factory update(Factory factory) {
        return null;
    }

    @Override
    public boolean delete(UUID id) {
        log.trace("Executing delete...");
        return factoryDao.removeById(id);
    }

    @Override
    public List<Factory> findAll() {
        log.trace("Executing findAll...");
        return factoryDao.find();
    }

    @Override
    public CalcResultDto calculate(double y, double t0, double t, double h) {
        log.trace("Executing calculate...");
        AUTO_CALC = true;
        int n = (int) ((t - t0) / h);
        List<Factory> factories = calcFactoriesP(n, t0, h);
        Map<Double, Double> calcY = calculateY(n, y, t0, h, factories);

        tempResultY.clear();

        CalcResultDto result = new CalcResultDto();
        result.setFactories(factories);
        result.setY(calcY);
        return result;
    }

    @Override
    public CalcResultDto autoCalc(double y, double t0, double t, double h) {
        log.trace("Executing autoCalc...");
        int n = (int) ((t - t0) / h);
        List<Factory> factories = calcFactoriesP(n, t0, h);
        calculateY(n, y, t0, h, factories);

        if (AUTO_CALC) {
            for (Factory factory : factories) {
                if (!factory.getTempResultP().get(0)
                        .equals(factory.getTempResultP().get(factory.getTempResultP().size() - 1))) {
                    AUTO_CORRECTION = true;
                    factory.setA(factory.getB());
                    factory.setE(factory.getB());
                    factoryDao.save(factory);
                }
            }
            if (!AUTO_CORRECTION) {
                if (!tempResultY.get(0).equals(tempResultY.get(tempResultY.size() - 1))) {
                    for (Factory factory : factories) {
                        factory.setE(factory.getA());
                        factoryDao.save(factory);
                    }
                }
            }
            AUTO_CALC = false;
        }

        tempResultY.clear();

        return calculate(y, t0, t, h);
    }

    @Override
    public Factory findByName(String factoryName) {
        if (StringUtils.hasText(factoryName)) {
            return factoryDao.findByName(factoryName);
        }
        return new Factory();
    }

    private List<Factory> calcFactoriesP(double n, double t0, double h) {
        List<Factory> factories = findAll();
        List<Factory> calculatedFactories = new LinkedList<>();

        double f1;
        double f2;
        double f3;
        double f4;

        double temp = t0;

        for (Factory factory : factories) {
            double originalP = factory.getP();
            for (int i = 1; i <= n; i++) {
                f1 = h * (Diff.dpdt(temp, factory.getP(), factory.getA(), factory.getB()));
                f2 = h * (Diff.dpdt(temp + 0.5 * h, factory.getP() + 0.5 * f1, factory.getA(), factory.getB()));
                f3 = h * (Diff.dpdt(temp + 0.5 * h, factory.getP() + 0.5 * f2, factory.getA(), factory.getB()));
                f4 = h * (Diff.dpdt(temp + h, factory.getP() + f3, factory.getA(), factory.getB()));

                factory.setP(factory.getP() + (1.0 / 6.0) * (f1 + 2 * f2 + 2 * f3 + f4));

                temp += h;

                Map<Double, Double> pToSave = factory.getResultP();
                pToSave.put((double) i, factory.getP());
                factory.setResultP(pToSave);

                List<Double> tempPToStore = factory.getTempResultP();
                tempPToStore.add(factory.getP());
                factory.setTempResultP(tempPToStore);
            }
            factory.setP(originalP);
            calculatedFactories.add(factory);
        }

        return calculatedFactories;
    }

    private Map<Double, Double> calculateY(double n, double y, double t0, double h, List<Factory> calculatedFactories) {
        double f1;
        double f2;
        double f3;
        double f4;

        double temp = t0;

        double c = calculatedFactories.stream().mapToDouble(factory -> factory.getAlpha() * factory.getA()).sum();

        Map<Double, Double> yToReturn = new ConcurrentHashMap<>();

        for (int i = 1; i <= n; i++) {
            f1 = h * (Diff.dydt(temp, y, c, calculatedFactories, i));
            f2 = h * (Diff.dydt(temp + 0.5 * h, y + 0.5 * f1, c, calculatedFactories, i));
            f3 = h * (Diff.dydt(temp + 0.5 * h, y + 0.5 * f2, c, calculatedFactories, i));
            f4 = h * (Diff.dydt(temp + h, y + f3, c, calculatedFactories, i));

            y = y + (1.0 / 6.0) * (f1 + 2 * f2 + 2 * f3 + f4);

            temp += h;

            yToReturn.put((double) i, y);

            tempResultY.add(y);
        }

        return yToReturn;
    }

    private static class Diff {
        public static double dpdt(double t, double p, double a, double b) {
            return (p * (a - b * p));
        }

        public static double dydt(double t, double y, double c, List<Factory> result, int index) {
            double sigma = result.stream().mapToDouble(factory -> factory.getE() * factory.getResultP().get((double) index)).sum();
            return (y * ((-c) + sigma));
        }
    }
}
