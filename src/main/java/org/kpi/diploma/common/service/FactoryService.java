package org.kpi.diploma.common.service;

import org.kpi.diploma.common.dto.CalcResultDto;
import org.kpi.diploma.common.model.Factory;

import java.util.List;
import java.util.UUID;

public interface FactoryService {
    Factory save(Factory factory);

    Factory update(Factory factory);

    boolean delete(UUID id);

    List<Factory> findAll();

    CalcResultDto calculate(double y, double t0, double t, double h);

    CalcResultDto autoCalc(double y, double t0, double t, double h);

    Factory findByName(String factoryName);
}
