package org.kpi.diploma.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Data
@EqualsAndHashCode
public class Factory implements Serializable {
    private UUID id;
    private String name;
    private double alpha;
    private double a;
    private double b;
    private double e;
    private double p;
    private Map<Double,  Double> resultP = new HashMap<>();

    @JsonIgnore
    private List<Double> tempResultP = new LinkedList<>();

    public Factory() {

    }

    public Factory(Factory factory) {
        this.id = factory.getId();
        this.name = factory.getName();
        this.alpha = factory.getAlpha();
        this.a = factory.getA();
        this.b = factory.getB();
        this.e = factory.getE();
        this.p  = factory.getP();
        this.resultP = factory.getResultP();
        this.tempResultP = factory.getTempResultP();
    }
}
