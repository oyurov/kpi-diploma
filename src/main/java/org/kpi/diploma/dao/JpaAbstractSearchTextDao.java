package org.kpi.diploma.dao;

import org.kpi.diploma.dao.model.SearchTextEntity;
import org.kpi.diploma.dao.model.sql.BaseEntity;

public abstract class JpaAbstractSearchTextDao <E extends BaseEntity<D>, D> extends JpaAbstractDao<E, D> {
    @Override
    protected void setSearchText(E entity) {
        ((SearchTextEntity) entity).setSearchText(((SearchTextEntity) entity).getSearchTextSource().toLowerCase());
    }
}
