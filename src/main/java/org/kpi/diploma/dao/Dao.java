package org.kpi.diploma.dao;

import java.util.List;
import java.util.UUID;

public interface Dao<T> {

    List<T> find();

    T findById(UUID id);

    T save(T t);

    boolean removeById(UUID id);
}
