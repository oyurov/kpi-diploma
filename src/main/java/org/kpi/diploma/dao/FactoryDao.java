package org.kpi.diploma.dao;

import org.kpi.diploma.common.model.Factory;

public interface FactoryDao extends Dao<Factory> {
    Factory findByName(String factoryName);
}
