package org.kpi.diploma.dao.sql;

import org.kpi.diploma.common.utils.AbstractListeningExecutor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class JpaExecutorService extends AbstractListeningExecutor {
    @Value("${spring.datasource.hikari.maximumPoolSize:10}")
    private int poolSize;

    @Override
    protected int getThreadPollSize() {
        return poolSize;
    }
}
