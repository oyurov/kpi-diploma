package org.kpi.diploma.dao.repository;

import org.kpi.diploma.dao.model.sql.FactoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface FactoryRepository extends JpaRepository<FactoryEntity, UUID> {

    FactoryEntity findByName(String factoryName);
}
