package org.kpi.diploma.dao.model.sql;

public interface ToData<T> {
    T toData();
}
