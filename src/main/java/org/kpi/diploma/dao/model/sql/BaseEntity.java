package org.kpi.diploma.dao.model.sql;

import java.util.UUID;

public interface BaseEntity<D> extends ToData<D> {
    UUID getId();

    void setId(UUID id);

    long getCreatedTime();

    void setCreatedTime(long createdTime);
}
