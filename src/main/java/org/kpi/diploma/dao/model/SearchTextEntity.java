package org.kpi.diploma.dao.model;

import org.kpi.diploma.dao.model.sql.BaseEntity;

public interface SearchTextEntity<D> extends BaseEntity<D> {
    String getSearchTextSource();

    void setSearchText(String searchText);
}
