package org.kpi.diploma.dao.model.sql;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.TypeDef;
import org.kpi.diploma.common.model.Factory;
import org.kpi.diploma.common.utils.mapping.JsonStringType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@TypeDef(name = "json", typeClass = JsonStringType.class)
@Table(name = "factory")
public final class FactoryEntity extends BaseSqlEntity<Factory> {

    @Column(name = "name")
    private String name;

    @Column(name = "alpha")
    private double alpha;

    @Column(name = "a")
    private double a;

    @Column(name = "b")
    private double b;

    @Column(name = "e")
    private double e;

    @Column(name = "p")
    private double p;

    public FactoryEntity() {
        super();
    }

    public FactoryEntity(Factory factory) {
        this.id = factory.getId();
        this.name = factory.getName();
        this.alpha = factory.getAlpha();
        this.a = factory.getA();
        this.b = factory.getB();
        this.e = factory.getE();
        this.p = factory.getP();
    }

    @Override
    public Factory toData() {
        Factory factory = new Factory();
        factory.setId(id);
        factory.setName(name);
        factory.setAlpha(alpha);
        factory.setA(a);
        factory.setB(b);
        factory.setE(e);
        factory.setP(p);
        return factory;
    }
}
