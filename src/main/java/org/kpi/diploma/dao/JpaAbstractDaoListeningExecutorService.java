package org.kpi.diploma.dao;

import org.kpi.diploma.dao.sql.JpaExecutorService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class JpaAbstractDaoListeningExecutorService {

    @Autowired
    protected JpaExecutorService service;
}
