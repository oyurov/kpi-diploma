package org.kpi.diploma.dao;

import org.kpi.diploma.dao.model.sql.FactoryEntity;
import org.kpi.diploma.dao.repository.FactoryRepository;
import org.kpi.diploma.common.model.Factory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class JpaFactoryDao extends JpaAbstractSearchTextDao<FactoryEntity, Factory> implements FactoryDao {

    @Autowired
    private FactoryRepository factoryRepository;

    @Override
    protected Class<FactoryEntity> getEntityClass() {
        return FactoryEntity.class;
    }

    @Override
    protected CrudRepository<FactoryEntity, UUID> getCrudRepository() {
        return factoryRepository;
    }

    @Override
    public Factory findByName(String factoryName) {
        return factoryRepository.findByName(factoryName).toData();
    }
}
